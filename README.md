# The project is a toy test task for Junior CV engineer at Enter Ideas

The task was to create a classification project that classifies presense Saint George on a given image.

My implementation includes image downloading, creating a dataframe with the image and class names, creating train and valid datasets, dataloaders and eventually finetuning the head of resnet50 model from torchvision.

The final accuracy score on validation set is 0.858.

The images, dataframe, run logs, metrics plot and final model can be obtained with the link on [Google Drive](https://drive.google.com/drive/folders/1LlfSWwaHwPMa7TTAUFgDyU2F9g-v_UBy?usp=sharing)

The room for improvements is very big. It can be bigger model, harder augmentations(i.e. mixup, cutmix), use kfold, ensemble several models, higher image resolution, label smoothing and so on.
